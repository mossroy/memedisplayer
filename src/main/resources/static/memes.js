var slideshow;
var accept;

var messageModalEl = document.getElementById('messageModal');
var messageModal = new bootstrap.Modal(messageModalEl);

var cookiesBarEl = document.getElementById('cookies-bar');
var cookiesBarTextEl = document.getElementById('cookies-bar-text');

var cookiesModalEl = document.getElementById('cookieDetails');
var cookiesModal = new bootstrap.Modal(cookiesModalEl);

// Configure Refresh button
var refreshButtonEl = document.getElementById('refresh');
refreshButtonEl.addEventListener('click', function() {

	// Call the backend and launch the slideshow
	fetch('/meme').then( response => response.json())
	.then( memeList => {
		var urlList = memeList.map( meme => meme.url);
		var idList = memeList.map( meme => meme.id);
		var myVotedMemeIds = memeList.filter( meme => meme.myVote ).map( meme => meme.id );
		var voteCounts = memeList.map( meme => meme.voteCount );
		slideshow?.stop();
		slideshow = {
			container: '#slideshow-container',
			media: urlList,
			ids: idList,
			myVotedMemeIds: myVotedMemeIds,
			voteCounts: voteCounts,
			folder: '',
			speed: 5000,
			autoplay: 'yes',
			stop : () => {},
			change(counter) {
				voteButtonEl.hidden = false;
				updateVoteCheckboxLabel(counter);
				voteChkEl.checked = slideshow.myVotedMemeIds.includes(this.ids[counter]);
			}
		}
		launchSlideShow();
	}).catch( error => {
		// Display error
		document.getElementById("result").textContent = error;
		messageModal.show(messageModalEl);
	});
});

// Read the memes on startup
window.onload=function() {
	refreshButtonEl.click();

	// Call backend to check cookie
	if (!accept) {
		fetch('/meme/check').then(response => {
			accept = response.status === 200;
			if (!accept) {
				cookiesBarEl.hidden = false;
			}
		});
	}

	// And automatically refresh them every 5 minutes
	setTimeout(function() {
		refreshButtonEl.click();
	}, 5*60*1000);
	
	// Check if the "add" anchor has been set in the URL
	var hash = window.location.hash? window.location.hash.substr(1) : null;
	if (hash === 'add') {
		// If so, directly open the window to add a meme
		document.getElementById('add').click();
	}
}

// Configure new Meme button
var addModalEl = document.getElementById('addModal');
var addModal = new bootstrap.Modal(addModalEl);
var newMemeButtonEl = document.getElementById('newMeme');
newMemeButtonEl.addEventListener('click', function() {
	var cocEl = document.getElementById('coc')
	if (! cocEl.checked) {
		alert("Il faut confirmer le respect du code de conduite");
		return;
	}
    addModal.hide(addModalEl);
    fetch('/meme', {
		method: 'PUT',
		headers: {'Content-Type': 'application/json;charset=UTF-8'},
		body: JSON.stringify({url: document.getElementById('url').value, email: document.getElementById('email').value})
	})
	.then( response => {
		if (response.status === 201) {
			document.getElementById('url').value = "";
			return "URL ajoutée, merci";
		} else if (response.status === 200) {
			document.getElementById('url').value = "";
			return "Cette URL existait déjà";
		} else {
			return response.text();
		}
	})
	.then( result => {
		// Display message
		document.getElementById("result").textContent = result;
		messageModal.show(messageModalEl);
		refreshButtonEl.click();
	}).catch( error => {
		// Display error
		document.getElementById("result").textContent = error;
		messageModal.show(messageModalEl);
	});
});

function alertCookiesVote() {
	cookiesBarEl.hidden = false;
	cookiesBarEl.className = "alert alert-warning";
	cookiesBarTextEl.innerText = "Les votes ne sont possibles qu'en acceptant les cookies"
}

function acceptCookies() {
	cookiesBarEl.hidden = true;
	fetch('/meme/accept', { method: 'POST' } )
		.then( response => { accept = response.status === 200 })
		.catch( error => {
			// Display error
			document.getElementById("result").textContent = error;
			cookiesBarEl.hidden = false;
		});
}

// Configure cookie-bar details Button
document.getElementById('cookies-bar-details').addEventListener('click', () => cookiesModal.show(cookiesModalEl));
// Configure cookie-bar close Button
document.getElementById('cookies-bar-close').addEventListener('click', () => cookiesBarEl.hidden = true);
// Configure cookie-bar ok Button
document.getElementById('cookies-bar-ok').addEventListener('click', acceptCookies);

// Configure new cookie accept Button
var acceptButtonEl = document.getElementById('accept');
acceptButtonEl.addEventListener('click', acceptCookies);

document.getElementById('refuse').addEventListener('click', () => cookiesBarEl.hidden = true);


// Configure vote button
var voteChkEl = document.getElementById('vote');
var voteButtonEl = document.getElementById('vote-label');
function updateVoteCheckboxLabel(counter) {
	voteButtonEl.innerText = 'Vote +1 ce meme (' + slideshow.voteCounts[counter] + ')';
}
voteButtonEl.addEventListener('click', function() {
	let meme = document.querySelector('.slideshow-info')?.textContent;
	if (meme) {
		if (voteChkEl.checked) {
			fetch(`/meme/unvote/${meme}`, {method: 'POST'})
				.then(response => {
					if (response.status === 403) {
						alertCookiesVote();
					}
					if (response.status === 200) {
						let index;
						index = slideshow.ids.indexOf(parseInt(meme));
						if (index !== -1) {
							slideshow.voteCounts[index] = slideshow.voteCounts[index] - 1;
							updateVoteCheckboxLabel(index);
						}
						index = slideshow.myVotedMemeIds.indexOf(parseInt(meme));
						if (index !== -1) {
							slideshow.myVotedMemeIds.splice(index, 1);
						}
					}
				})
				.catch(error => {
					// Display error
					document.getElementById("result").textContent = error;
					messageModal.show(messageModalEl);
				});
		} else {
			fetch(`/meme/vote/${meme}`, {method: 'POST'})
				.then(response => {
					if (response.status === 403) {
						alertCookiesVote();
					}
					if (response.status === 201) {
						let index;
						index = slideshow.ids.indexOf(parseInt(meme));
						if (index !== -1) {
							slideshow.voteCounts[index] = slideshow.voteCounts[index] + 1;
							updateVoteCheckboxLabel(index);
						}
						index = slideshow.myVotedMemeIds.indexOf(parseInt(meme));
						if (index === -1) {
							slideshow.myVotedMemeIds.push(parseInt(meme));
						}
					}
				})
				.catch(error => {
					// Display error
					document.getElementById("result").textContent = error;
					messageModal.show(messageModalEl);
				});
		}
	}
});