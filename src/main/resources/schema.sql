create table if not exists meme(
	id serial primary key,
	url text not null,
	email text,
	ip text not null
);
create table if not exists blacklisted_ip(
	id serial primary key,
	ip text not null
);
CREATE TABLE IF NOT EXISTS vote(
    user_id TEXT NOT NULL,
    meme_id SERIAL NOT NULL,
    CONSTRAINT fk_vote_meme FOREIGN KEY (meme_id) REFERENCES meme(id),
    CONSTRAINT vote_user_meme_unique UNIQUE (user_id, meme_id)
);

CREATE INDEX IF NOT EXISTS vote_user_idx ON vote USING BTREE (user_id);
CREATE INDEX IF NOT EXISTS vote_meme_idx ON vote USING BTREE (meme_id);

alter table meme add column if not exists user_agent text;
alter table meme add column if not exists last_update timestamp;
alter table meme add column if not exists deleted boolean not null default false;

alter table vote add column if not exists ip text;
alter table vote add column if not exists user_agent text;
alter table vote add column if not exists last_update timestamp;
alter table vote add column if not exists deleted boolean not null default false;
