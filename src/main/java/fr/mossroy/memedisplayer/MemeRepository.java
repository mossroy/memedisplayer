package fr.mossroy.memedisplayer;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

@Repository
public class MemeRepository {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MemeRepository.class);
	
	@Autowired  
    private JdbcTemplate jdbc;
	
	public long addMeme(Meme meme) {
		int count = jdbc.queryForObject("select count(*) from meme where url=?", new Object[] { meme.getUrl() }, new int[] { Types.VARCHAR }, Integer.class);
		// Don't insert the same URL twice
		if (count == 0) {
			SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbc);
			jdbcInsert.withTableName("meme").usingGeneratedKeyColumns("id");
			Map<String, Object> parameters = new HashMap<>();
	        parameters.put("url", meme.getUrl());
	        parameters.put("email", meme.getEmail());
	        parameters.put("ip", meme.getIp());
	        parameters.put("user_agent", meme.getUserAgent());
	        parameters.put("last_update", meme.getLastUpdate());
	        parameters.put("deleted", meme.isDeleted());
			Number insertedId = jdbcInsert.executeAndReturnKey(new MapSqlParameterSource(parameters));
			return insertedId.longValue();
		}
		return -1;
	}
	
	public List<Meme> listMemes() {
		return jdbc.queryForList("select * from meme where deleted = false order by id desc")
				.stream()
				.map(memeRecord -> {
					Meme meme = new Meme();
					meme.setId(Long.parseLong(String.valueOf(memeRecord.get("id"))));
					meme.setUrl(String.valueOf(memeRecord.get("url")));
					meme.setEmail(String.valueOf(memeRecord.get("email")));
					meme.setIp(String.valueOf(memeRecord.get("ip")));
					meme.setUserAgent(String.valueOf(memeRecord.get("user_agent")));
					Timestamp lastUpdateTimestamp = (Timestamp) memeRecord.get("last_update");
					meme.setLastUpdate(lastUpdateTimestamp != null? lastUpdateTimestamp.toLocalDateTime() : null);
					return meme;
				})
				.toList();
	}

	public boolean exists(long id) {
		return jdbc.queryForObject("select exists(select 1 from meme where id=?)", new Object[] { id }, new int[] { Types.NUMERIC }, boolean.class);
	}

	public boolean isIPBlacklisted(String ip) {
		return jdbc.queryForObject("select exists(select 1 from blacklisted_ip where ip=?)", new Object[] { ip }, new int[] { Types.VARCHAR }, boolean.class);
	}
	
	public boolean addBlacklistedIP(String ip) {
		return jdbc.execute("insert into blacklisted_ip (ip) values(?)", new PreparedStatementCallback<Boolean>() {
			@Override
			public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
				ps.setString(1, ip);
				return ps.execute();
			}
		});
	}

	public void flush() {
		jdbc.execute("delete from blacklisted_ip");
		jdbc.execute("delete from meme");
	}
	
}	
