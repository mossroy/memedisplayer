package fr.mossroy.memedisplayer;

import java.time.LocalDateTime;

public class Vote {
    private final String userId;
    private final long memeId;
	private final String ip;
	private final String userAgent;
	private final LocalDateTime lastUpdate;
	private final boolean deleted;

    public Vote(String user, long memeId, String ip, String userAgent, LocalDateTime lastUpdate, boolean deleted) {
        this.userId = user;
        this.memeId = memeId;
		this.ip = ip;
		this.userAgent = userAgent;
		this.lastUpdate = lastUpdate;
		this.deleted = deleted;
    }

    public String getUserId() {
        return userId;
    }

    public long getMemeId() {
        return memeId;
    }

	public String getIp() {
		return ip;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}

	public boolean isDeleted() {
		return deleted;
	}
    
}
