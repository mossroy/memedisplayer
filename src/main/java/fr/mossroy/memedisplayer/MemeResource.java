package fr.mossroy.memedisplayer;

import static java.lang.String.format;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/meme")
public class MemeResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(MemeResource.class);
	private static final String SESSION_COOKIE_NAME = "MSESSION";
	private static final int SESSION_COOKIE_MAX_AGE = 60 * 60 * 24 * 30; // One month in second

    @Autowired
    private MemeRepository memeRepository;
	@Autowired
	private VoteRepository voteRepository;

	@GetMapping()
	public List<MemeDTO> getAllMemes(HttpServletRequest request) {
		Map<Long, Long> voteCounts = voteRepository.countByMemeId();
		Set<Long> userVotes = cookie(request, SESSION_COOKIE_NAME).map(voteRepository::voteByUser).orElse(emptyList())
				.stream().map(Vote::getMemeId).collect(Collectors.toSet());
		return memeRepository.listMemes().stream().map(meme -> new MemeDTO(
				meme.getId(),
				meme.getUrl(),
				userVotes.contains(meme.getId()),
				voteCounts.get(meme.getId())
		)).toList();
	}
	
	@PostMapping(path = "/accept")
	public void register(HttpServletRequest request, HttpServletResponse response) {
		if (!cookie(request, SESSION_COOKIE_NAME).isPresent()) {
			Cookie cookie = new Cookie(SESSION_COOKIE_NAME, UUID.randomUUID().toString());
			cookie.setSecure(true);
			cookie.setHttpOnly(true);
			cookie.setMaxAge(SESSION_COOKIE_MAX_AGE);
			response.addCookie(cookie);
		}
	}

	@GetMapping(path = "/check")
	public ResponseEntity<String> isRegistered(HttpServletRequest request) {
		return cookie(request, SESSION_COOKIE_NAME)
				.map(x -> new ResponseEntity<>("", HttpStatus.OK))
				.orElse(new ResponseEntity<>("", HttpStatus.FORBIDDEN));
	}

	@PostMapping(path = "/vote/{id}")
	public ResponseEntity<String> vote(@PathVariable long id, HttpServletRequest request) {
		String ip = request.getRemoteAddr();
		if (memeRepository.isIPBlacklisted(ip)) {
			LOGGER.info("IP {} blocked when trying to vote", ip);
			return new ResponseEntity<>("IP blacklistée", HttpStatus.BAD_REQUEST);
		}
		
		Optional<String> session = cookie(request, SESSION_COOKIE_NAME);
		if (session.isEmpty()) {
			return new ResponseEntity<>("Politique refusée : action impossible", HttpStatus.FORBIDDEN);
		}
		if (!memeRepository.exists(id)) {
			return new ResponseEntity<>("Meme non trouvé", HttpStatus.NOT_FOUND);
		}
		
		String userAgent = request.getHeader(HttpHeaders.USER_AGENT);

		try {
			boolean created = voteRepository.add(new Vote(session.get(), id, ip, userAgent, LocalDateTime.now(), false));
			if (created) {
				return new ResponseEntity<>(HttpStatus.CREATED);
			}
		} catch (DuplicateKeyException e) {
			// bdd confict like created = false
		} catch (Exception e) {
			LOGGER.error(format("Erreur lors du vote de user:%s meme:%d", session, id), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.CONFLICT);
	}

	@PostMapping(path = "/unvote/{id}")
	public ResponseEntity<String> unvote(@PathVariable long id, HttpServletRequest request) {
		String ip = request.getRemoteAddr();
		if (memeRepository.isIPBlacklisted(ip)) {
			LOGGER.info("IP {} blocked when trying to vote", ip);
			return new ResponseEntity<>("IP blacklistée", HttpStatus.BAD_REQUEST);
		}
		
		Optional<String> session = cookie(request, SESSION_COOKIE_NAME);
		if (session.isEmpty()) {
			return new ResponseEntity<>("Politique refusée : action impossible", HttpStatus.FORBIDDEN);
		}
		if (!memeRepository.exists(id)) {
			return new ResponseEntity<>("Meme non trouvé", HttpStatus.NOT_FOUND);
		}
		
		String userAgent = request.getHeader(HttpHeaders.USER_AGENT);

		try {
			boolean deleted = voteRepository.delete(new Vote(session.get(), id, ip, userAgent, LocalDateTime.now(), false));
			if (deleted) {
				return new ResponseEntity<>(HttpStatus.OK);
			}
		} catch (Exception e) {
			LOGGER.error(format("Erreur lors du unvote de user:%s meme:%d", session, id), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.CONFLICT);
	}

	@PutMapping()
	public ResponseEntity<String> addMeme(@RequestBody Meme meme, HttpServletRequest request) {
		meme.setIp(request.getRemoteAddr());
		
		if (memeRepository.isIPBlacklisted(meme.getIp())) {
			LOGGER.info("IP {} blocked when trying to upload a Meme", meme.getIp());
			return new ResponseEntity<>("IP blacklistée", HttpStatus.BAD_REQUEST);
		}
		
		meme.setUserAgent(request.getHeader(HttpHeaders.USER_AGENT));
		meme.setLastUpdate(LocalDateTime.now());
		meme.setDeleted(false);
		
		LOGGER.info("URL {} suggested by {} (IP {})", meme.getUrl(), meme.getEmail(), meme.getIp());

		// Convert imgflip URL
		meme.convertHTMLUrl();

		// Control it's a valid URL
		try {
			if (!meme.isHttpsScheme()) {
				return new ResponseEntity<>("URL proposée invalide : l'URL doit être en https", HttpStatus.BAD_REQUEST);
			}
			int statusCode = meme.getURLStatus();
			if (statusCode<200 || statusCode>=400) {
				return new ResponseEntity<>("URL proposée invalide : statusCode " + statusCode, HttpStatus.BAD_REQUEST);
			}
		} catch (IOException e) {
			return new ResponseEntity<>("URL proposée invalide : " + e.getClass() + " " + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		// Control it's an image or a video
		try {
			if (! meme.isImageOrVideo()) {
				return new ResponseEntity<>("URL proposée invalide : ce n'est ni une image ni une vidéo (ni une page web qu'on saurait convertir en image ou vidéo)", HttpStatus.BAD_REQUEST);
			}
		} catch (IOException e) {
			return new ResponseEntity<>("URL proposée invalide : " + e.getClass() + " " + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		long createdId = memeRepository.addMeme(meme);
		if (createdId >= 0) {
			return new ResponseEntity<>(HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}

	private Optional<String> cookie(HttpServletRequest request, String name) {
		return request.getCookies() == null ?
				Optional.empty()
				: stream(request.getCookies()).filter(c -> c.getName().equals(name)).map(Cookie::getValue).findAny();
	}


}