package fr.mossroy.memedisplayer;

/**
 * Data Transfer Object for a Meme, with only the necessary data to display it 
 * 
 * @author mossroy
 *
 */
public class MemeDTO {
	
	private long id;
	private String url;
	private boolean myVote;
	private long voteCount;

	public MemeDTO() {
		super();
	}

	public MemeDTO(long id, String url, boolean myVote, long voteCount) {
		this.id = id;
		this.url = url;
		this.myVote = myVote;
		this.voteCount = voteCount;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public boolean isMyVote() {
		return myVote;
	}
	public void setMyVote(boolean myVote) {
		this.myVote = myVote;
	}
	public long getVoteCount() {
		return voteCount;
	}
	public void setVoteCount(long voteCount) {
		this.voteCount = voteCount;
	}
}
