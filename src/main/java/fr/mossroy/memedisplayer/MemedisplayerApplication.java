package fr.mossroy.memedisplayer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemedisplayerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MemedisplayerApplication.class, args);
	}

}
