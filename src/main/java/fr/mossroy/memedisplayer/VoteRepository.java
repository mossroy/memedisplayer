package fr.mossroy.memedisplayer;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Repository;

@Repository
public class VoteRepository {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VoteRepository.class);
	
	@Autowired  
    private JdbcTemplate jdbc;
	
	public boolean add(Vote vote) {
		int count = jdbc.queryForObject("select count(*) from vote where user_id=? and meme_id=?", new Object[] { vote.getUserId(), vote.getMemeId() }, new int[] { Types.VARCHAR, Types.NUMERIC }, Integer.class);
		// Don't insert the same vote twice
		if (count == 0) {
			jdbc.execute("insert into vote (user_id, meme_id, ip, user_agent, last_update, deleted) values(?,?,?,?,?,?)", new PreparedStatementCallback<Boolean>() {
				@Override
				public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
					ps.setString(1, vote.getUserId());
					ps.setLong(2, vote.getMemeId());
					ps.setString(3, vote.getIp());
					ps.setString(4, vote.getUserAgent());
					ps.setTimestamp(5, vote.getLastUpdate() != null ? Timestamp.valueOf(vote.getLastUpdate()) : null);
					ps.setBoolean(6, vote.isDeleted());
					return ps.execute();
				}

			});
			return true;
		}
		return false;
	}

	public boolean delete(Vote vote) {
		int count = jdbc.update("delete from vote where user_id=? and meme_id=?", new Object[] { vote.getUserId(), vote.getMemeId() }, new int[] { Types.VARCHAR, Types.NUMERIC });
		return count > 0;
	}

	public Map<Long, Long> countByMemeId() {
		return jdbc.queryForList("select m.id as id, count(v.user_id) as votes from meme m left join vote v on m.id = v.meme_id and v.deleted = false where m.deleted = false group by m.id")
				.stream()
				.collect(Collectors.toMap(
					memeRecord -> Long.parseLong(String.valueOf(memeRecord.get("id"))),
					memeRecord -> Long.parseLong(String.valueOf(memeRecord.get("votes")))
				));
	}

	public List<Vote> voteByUser(String userId) {
		return jdbc.queryForList("select * from vote where user_id = ? and deleted = false", new Object[] { userId }, new int[] { Types.VARCHAR })
				.stream()
				.map(memeRecord -> new Vote(userId,
						Long.parseLong(String.valueOf(memeRecord.get("meme_id"))),
						String.valueOf(memeRecord.get("ip")),
						String.valueOf(memeRecord.get("user_agent")),
						memeRecord.get("last_update") != null ? ((Timestamp) memeRecord.get("last_update")).toLocalDateTime() : null,
						false))
				.toList();
	}
	
	public void flush() {
		jdbc.execute("delete from vote");
	}
}
