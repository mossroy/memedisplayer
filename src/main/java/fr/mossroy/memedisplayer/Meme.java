package fr.mossroy.memedisplayer;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Meme {
	
	private static final Pattern PATTERN_IMGFLIP_JPEG = Pattern.compile("^https://imgflip.com/i/(.*)$", Pattern.CASE_INSENSITIVE);
	private static final Pattern PATTERN_IMGFLIP_GIF = Pattern.compile("^https://imgflip.com/gif/(.*)$", Pattern.CASE_INSENSITIVE);
	private static final Pattern PATTERN_MAKEAMEME_JPEG = Pattern.compile("^https://makeameme.org/meme/(.*)$", Pattern.CASE_INSENSITIVE);
	
	private long id;
	private String url;
	private String email;
	private String ip;
	private String userAgent;
	private LocalDateTime lastUpdate;
	private boolean deleted;

	public Meme() {
		super();
	}
	
	public Meme(String url, String email, String ip, String userAgent, LocalDateTime lastUpdate, boolean deleted) {
		super();
		this.url = url;
		this.email = email;
		this.ip = ip;
		this.userAgent = userAgent;
		this.lastUpdate = lastUpdate;
		this.deleted = deleted;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * Convert an HTML URL to its corresponding image/video
	 */
	public void convertHTMLUrl() {
		// Convert imgflip static images
		Matcher matcher = PATTERN_IMGFLIP_JPEG.matcher(url);
		String modifiedUrl = matcher.replaceFirst("https://i.imgflip.com/$1.jpg");
		// Convert imgflip animated GIFs
		matcher = PATTERN_IMGFLIP_GIF.matcher(modifiedUrl);
		modifiedUrl = matcher.replaceFirst("https://i.imgflip.com/$1.mp4");
		// Convert makeameme static images
		matcher = PATTERN_MAKEAMEME_JPEG.matcher(modifiedUrl);
		modifiedUrl = matcher.replaceFirst("https://media.makeameme.org/created/$1.jpg");
		url = modifiedUrl;
	}

	/**
	 * Tells if the URL is using HTTPS scheme
	 * 
	 * @return
	 */
	public boolean isHttpsScheme() throws IOException {
		URL url = new URL(this.url);
		String protocol = url.getProtocol();
		return "https".equalsIgnoreCase(protocol);
	}

	/**
	 * Gets the HTTP status of the URL
	 *
	 * @return HTTP status code
	 * @throws IOException
	 */
	public int getURLStatus() throws IOException {
		URL url = new URL(this.url);
		HttpURLConnection huc = (HttpURLConnection) url.openConnection();
		int responseCode = huc.getResponseCode();
		huc.disconnect();
		return responseCode;
	}
	
	/**
	 * Gets the MIME-Type of the URL
	 *
	 * @return MIME-Type string
	 * @throws IOException
	 */
	private String getMimeType() throws IOException {
		URL url = new URL(this.url);
		HttpURLConnection huc = (HttpURLConnection) url.openConnection();
		String contentType = huc.getContentType();
		huc.disconnect();
		return contentType;
	}
	
	public boolean isImage() throws IOException {
		String mimeType = getMimeType();
		return (mimeType.startsWith("image"));
	}
	
	public boolean isVideo() throws IOException {
		String mimeType = getMimeType();
		return (mimeType.startsWith("video"));
	}
	
	public boolean isImageOrVideo() throws IOException {
		String mimeType = getMimeType();
		return (mimeType.startsWith("image") || mimeType.startsWith("video"));
	}
}
