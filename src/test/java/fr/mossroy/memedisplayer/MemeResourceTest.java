package fr.mossroy.memedisplayer;

import static org.assertj.core.api.Assertions.assertThat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.cookie;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.function.Consumer;

import jakarta.servlet.http.Cookie;

import org.assertj.core.api.Assertions;
import org.assertj.core.groups.Tuple;
import org.assertj.core.matcher.AssertionMatcher;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@ActiveProfiles("test")
@AutoConfigureMockMvc
@SpringBootTest
@SqlGroup({
        @Sql(value = "memeResourceTestGiven.sql"),
        @Sql(value = "memeResourceTestClean.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
})
class MemeResourceTest {

    @Autowired
    private MockMvc mockMvc;
    
	@Autowired
	private MemeRepository memeRepository;
	
	@Autowired
	private VoteRepository voteRepository;

    private static final Cookie MSESSION_COOKIE = new Cookie("MSESSION", "123456789");
    private static final Cookie OTHER_MSESSION_COOKIE = new Cookie("MSESSION", "987654321");
    private static final String JSON_ADD_MEME = "{\"url\":\"https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png\",\"email\":\"a@a.fr\"}";

    @Test
    void shouldRegister() throws Exception { 
        mockMvc.perform(post("/meme/accept"))
                .andExpect(status().isOk())
                .andExpect(cookie().exists("MSESSION"));
    }

    @Test
    void shouldNotRegisterTwice() throws Exception {
        mockMvc.perform(post("/meme/accept").cookie(MSESSION_COOKIE))
                .andExpect(status().isOk())
                .andExpect(cookie().doesNotExist("MSESSION"));
    }

    @Test
    void shouldCheckFailedWhenCookieNotPresent() throws Exception {
        mockMvc.perform(get("/meme/check"))
                .andExpect(status().isForbidden());
    }

    @Test
    void shouldCheckOkWhenCookieIsPresent() throws Exception {
        mockMvc.perform(get("/meme/check").cookie(MSESSION_COOKIE))
                .andExpect(status().isOk());
    }

    @Test
    void shouldUnregisteredVoteFailedWhenCookieNotPresent() throws Exception {
        mockMvc.perform(post("/meme/vote/1000"))
                .andExpect(status().isForbidden());
    }

    @Test
    void shouldVoteFailedWhenInvalidMeme() throws Exception {
        mockMvc.perform(post("/meme/vote/1001").cookie(MSESSION_COOKIE))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldVoteSuccessWhenRegisteredAndValidMeme() throws Exception {
        mockMvc.perform(post("/meme/vote/1000").cookie(MSESSION_COOKIE))
                .andExpect(status().isCreated());
    }

    @Test
    void shouldVoteFailedWhenTwice() throws Exception {
        mockMvc.perform(post("/meme/vote/2000").cookie(MSESSION_COOKIE))
                .andExpect(status().isConflict());
    }

    @Test
    void shouldUnvoteFailedWhenUnregistered() throws Exception {
        mockMvc.perform(post("/meme/unvote/2000"))
                .andExpect(status().isForbidden());
    }

    @Test
    void shouldUnvoteFailedWhenInvalidMeme() throws Exception {
        mockMvc.perform(post("/meme/unvote/1001").cookie(MSESSION_COOKIE))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldUnvoteSuccessWhenRegisteredAndValidMeme() throws Exception {
        mockMvc.perform(post("/meme/unvote/2000").cookie(MSESSION_COOKIE))
                .andExpect(status().isOk());
    }

    @Test
    void shouldUnvoteFailedWhenTwice() throws Exception {
        mockMvc.perform(post("/meme/unvote/1000").cookie(MSESSION_COOKIE))
                .andExpect(status().isConflict());
    }

    @Test
    void shouldReturnMemeWithVoteCount() throws Exception {
        mockMvc.perform(get("/meme"))
                .andExpect(status().isOk())
                .andExpect(content().string(checkMeme(
                        memes -> Assertions.assertThat(memes).extracting(
                                MemeDTO::getId, MemeDTO::getUrl, MemeDTO::getVoteCount, MemeDTO::isMyVote
                        ).contains(
                                Tuple.tuple(1000L, "https://image1.svg", 0L, false),
                                Tuple.tuple(2000L, "https://image2.svg", 1L, false)
                        )
                )));
    }

    @Test
    void shouldReturnMemeWithOwnVoteCount() throws Exception {
        mockMvc.perform(get("/meme").cookie(MSESSION_COOKIE))
                .andExpect(status().isOk())
                .andExpect(content().string(checkMeme(
                        memes -> Assertions.assertThat(memes).extracting(
                                MemeDTO::getId, MemeDTO::getUrl, MemeDTO::getVoteCount, MemeDTO::isMyVote
                        ).contains(
                                Tuple.tuple(1000L, "https://image1.svg", 0L, false),
                                Tuple.tuple(2000L, "https://image2.svg", 1L, true)
                        )
                )));
    }

    private Matcher<? super String> checkMeme(Consumer<List<MemeDTO>> memeAssertions) {
        return new AssertionMatcher<>() {
            @Override
            public void assertion(String content) throws AssertionError {
                try {
                    memeAssertions.accept(new ObjectMapper().readValue(content, new TypeReference<List<MemeDTO>>(){}));
                } catch (JsonProcessingException e) {
                    Assertions.fail(e.getMessage(), e);
                }
            }
        };
    }
    
    @Test
    void addMemeShouldBeBlockedIfIpBlacklisted() throws Exception {
    	mockMvc.perform(put("/meme")
    			.contentType("application/json")
    			.content(JSON_ADD_MEME)
    			.with(request->{request.setRemoteAddr("1.2.3.0");return request;}))
    		.andExpect(status().isBadRequest());
    }
    
    @Test
    void addMemeShouldWorkIfIpNotBlacklisted() throws Exception {
    	mockMvc.perform(put("/meme")
    			.contentType("application/json")
    			.content(JSON_ADD_MEME)
    			.with(request->{request.setRemoteAddr("1.2.3.1");return request;}))
    		.andExpect(status().is2xxSuccessful());
    }
    
    @Test
    void addMemeShouldStoreIPUserAgentAndTimestamp() throws Exception {
    	assertThat(memeRepository.listMemes()).extracting("ip").doesNotContain("1.2.3.2");
    	mockMvc.perform(put("/meme")
    			.contentType("application/json")
    			.content(JSON_ADD_MEME)
    			.with(request->{request.setRemoteAddr("1.2.3.2");return request;})
    			.header(HttpHeaders.USER_AGENT, "FirefoxUserAgent"))
    		.andExpect(status().is2xxSuccessful());
    	List<Meme> allMemes = memeRepository.listMemes();
    	assertThat(allMemes).extracting(Meme::getIp,Meme::getUserAgent).contains(Tuple.tuple("1.2.3.2","FirefoxUserAgent"));
    	assertThat(allMemes).filteredOn(Meme::getIp, "1.2.3.2").extracting("lastUpdate").allSatisfy(localDateTime -> assertThat(localDateTime).isNotNull());
    }
    
    @Test
    void addVoteShouldBeBlockedIfIpBlacklisted() throws Exception {
        mockMvc.perform(post("/meme/vote/1000").cookie(OTHER_MSESSION_COOKIE)
   			.with(request->{request.setRemoteAddr("1.2.3.0");return request;}))
    		.andExpect(status().isBadRequest());
    }
    
    @Test
    void addVoteShouldWorkIfIpNotBlacklisted() throws Exception {
        mockMvc.perform(post("/meme/vote/1000").cookie(OTHER_MSESSION_COOKIE)
   			.with(request->{request.setRemoteAddr("1.2.3.1");return request;}))
    		.andExpect(status().is2xxSuccessful());
    }
    
    @Test
    void addVoteShouldStoreIPUserAgentAndTimestamp() throws Exception {
    	assertThat(voteRepository.voteByUser("987654321")).extracting("ip").doesNotContain("1.2.3.2");
        mockMvc.perform(post("/meme/vote/1000").cookie(OTHER_MSESSION_COOKIE)
   			.with(request->{request.setRemoteAddr("1.2.3.2");return request;})
        	.header(HttpHeaders.USER_AGENT, "ChromeUserAgent"))
    		.andExpect(status().is2xxSuccessful());
        List<Vote> myVotes = voteRepository.voteByUser("987654321");
        assertThat(myVotes).extracting(Vote::getIp,Vote::getUserAgent).contains(Tuple.tuple("1.2.3.2","ChromeUserAgent"));
        assertThat(myVotes).filteredOn(Vote::getIp, "1.2.3.2").extracting("lastUpdate").allSatisfy(localDateTime -> assertThat(localDateTime).isNotNull());
    }
    

}
