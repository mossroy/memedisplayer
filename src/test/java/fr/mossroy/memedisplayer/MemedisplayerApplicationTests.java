package fr.mossroy.memedisplayer;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class MemedisplayerApplicationTests {
	
	private static final String IP = "1.2.3.4";
	private static final String USER_AGENT = "MyUserAgent";
	
	@Autowired
	private MemeRepository memeRepository;
	
	@Autowired
	private VoteRepository voteRepository;

	@Test
	void contextLoads() {
	}
	
	@BeforeEach
	void setUp() {
    	voteRepository.flush();
		memeRepository.flush();
	}
	
	@Test
	void singleMemeAddedShouldBeRetrievedAfterwards() {
		// PostgreSQL does not store as much precision as LocalDateTime, so we only test up to milliseconds
		LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS);
		memeRepository.addMeme(new Meme("http://test", "a@a.fr", IP, USER_AGENT, now, false));
		List<Meme> list = memeRepository.listMemes();
		assertThat(list).extracting("url","email","ip","userAgent","lastUpdate","deleted").containsExactly(Tuple.tuple("http://test","a@a.fr",IP,USER_AGENT,now,false));
	}
	
	@Test
	void duplicateMemeAddedShouldBeInsertedOnlyOnce() {
		assertThat(memeRepository.addMeme(new Meme("http://test", null, IP, null, null, false))).isPositive();
		assertThat(memeRepository.addMeme(new Meme("http://test", null, IP, null, null, false))).isNegative();
		List<Meme> list = memeRepository.listMemes();
		assertThat(list).extracting("url").containsExactly("http://test");
	}
	
	@Test
	void ipShouldNotBeBlacklistedIfNotInTable() {
		assertThat(memeRepository.isIPBlacklisted(IP)).isFalse();
	}

	@Test
	void ipShouldBeBlacklistedIfInTable() {
		memeRepository.addBlacklistedIP(IP);
		assertThat(memeRepository.isIPBlacklisted(IP)).isTrue();
	}
	
    @Test
    void logicallyDeletedMemesShouldBeIgnored() throws Exception {
    	memeRepository.addMeme(new Meme("http://test", null, IP, null, null, false));
    	memeRepository.addMeme(new Meme("http://test2", null, IP, null, null, true));
    	List<Meme> list = memeRepository.listMemes();
    	assertThat(list).extracting("url").containsExactly("http://test");
    }
    
    @Test
    void logicallyDeletedVotesShouldBeIgnored() throws Exception {
    	long myNewMemeId = memeRepository.addMeme(new Meme("http://test3", null, IP, null, null, false));
    	voteRepository.add(new Vote("123456", myNewMemeId, IP, null, null, false));
    	voteRepository.add(new Vote("654321", myNewMemeId, IP, null, null, true));
    	assertThat(voteRepository.voteByUser("123456")).isNotEmpty().extracting("memeId").contains(myNewMemeId);
    	assertThat(voteRepository.voteByUser("654321")).isEmpty();
    }
}
