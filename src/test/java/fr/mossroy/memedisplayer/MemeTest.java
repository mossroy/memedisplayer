package fr.mossroy.memedisplayer;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;

import org.junit.jupiter.api.Test;

public class MemeTest {

	@Test
	void invalidUrlShouldThrowException() {
		Meme meme = new Meme("htttp://invalidprotocol", null, null, null, null, false);
		assertThrows(IOException.class, () -> {
			meme.getURLStatus();
		});
	}
	
	@Test
	void httpsUrlShouldBeDetectedAsHttps() throws IOException {
		Meme meme = new Meme("https://www.google.com/gdklgdsjvjpogrtgrtjg", null, null, null, null, false);
		assertThat(meme.isHttpsScheme()).isTrue();
	}
	
	@Test
	void jarUrlShouldBeDetectedAsNotHttp() throws IOException {
		Meme meme = new Meme("ftp://ImTryingToDoNastyThings", null, null, null, null, false);
		assertThat(meme.isHttpsScheme()).isFalse();
	}
	
	@Test
	void urlNotFoundShouldHave404status() throws IOException {
		Meme meme = new Meme("https://www.google.com/gdklgdsjvjpogrtgrtjg", null, null, null, null, false);
		assertThat(meme.getURLStatus()).isEqualTo(404);
	}
	
	@Test
	void imgflipMemeUrlShouldBeConvertedtoJPEG() {
		Meme meme = new Meme("https://imgflip.com/i/6d6u3h", null, null, null, null, false);
		meme.convertHTMLUrl();
		assertThat(meme.getUrl()).isEqualTo("https://i.imgflip.com/6d6u3h.jpg");
	}
	
	@Test
	void imgflipGifUrlShouldBeConvertedToMP4() {
		Meme meme = new Meme("https://imgflip.com/gif/6d7wuo", null, null, null, null, false);
		meme.convertHTMLUrl();
		assertThat(meme.getUrl()).isEqualTo("https://i.imgflip.com/6d7wuo.mp4");
	}
	
	@Test
	void randomUrlShouldNotBeConverted() {
		Meme meme = new Meme("https://www.google.com", null, null, null, null, false);
		meme.convertHTMLUrl();
		assertThat(meme.getUrl()).isEqualTo("https://www.google.com");
	}
	
	@Test
	void makeamemeUrlShouldBeConvertedtoJPEG() {
		Meme meme = new Meme("https://makeameme.org/meme/blabla-hh", null, null, null, null, false);
		meme.convertHTMLUrl();
		assertThat(meme.getUrl()).isEqualTo("https://media.makeameme.org/created/blabla-hh.jpg");
	}
	
	@Test
	void imageURLShouldBeDetectedAsImage() throws IOException {
		Meme meme = new Meme("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png", null, null, null, null, false);
		assertThat(meme.isImage()).isTrue();
		assertThat(meme.isImageOrVideo()).isTrue();
		assertThat(meme.isVideo()).isFalse();
	}
	
	@Test
	void webpageURLShouldNotBeDetectedAsImageOrVideo() throws IOException {
		Meme meme = new Meme("https://www.google.com/", null, null, null, null, false);
		assertThat(meme.isImage()).isFalse();
		assertThat(meme.isImageOrVideo()).isFalse();
		assertThat(meme.isVideo()).isFalse();
	}
	
	@Test
	void videoURLShouldBeDetectedAsVideo() throws IOException {
		Meme meme = new Meme("https://upload.wikimedia.org/wikipedia/commons/transcoded/a/a7/How_to_make_video.webm/How_to_make_video.webm.480p.vp9.webm", null, null, null, null, false);
		assertThat(meme.isImage()).isFalse();
		assertThat(meme.isImageOrVideo()).isTrue();
		assertThat(meme.isVideo()).isTrue();
	}
}
