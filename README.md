# Meme Displayer

Simple webapp that allows to add image URLs to a list, and display them.

To be used to allow people to send Mèmes, and display them to everybody.

Users can also vote for their favorite mème, and there is a blacklist feature to block some users.
